module.exports = function (grunt){
	grunt.initConfig({
		sass: {
			dist:{
				files:[{
					expand: true,
					cwd: 'css',
					src: ['*.scss'],
					dest: 'css',
					ext: '.css'	
				}]
			}
		},

		watch:{
			files: ['css/*.scss'],
			tasks: ['css']
		},
		browserSync:{
			dev:{
				bsFiles: { //browser files
					src: [
					'css/*.css',
					'*.html',
					'js/*.js'
					]				
				},
			options: {
				wathTask: true,
				server:{
					baseDir: './' //Directorio base para uestro servidor
					}
				}
			},

			imagemin:{
				dynamic:{
					files:[{
						expand: true,
						cwd: './',
						src: 'img/+.{png,gif,jpg,jpeg}',
						dest: 'dist/'
					}]
				}
			}
	},

	});

	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-browser-sync');
	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.resgisterTasks('css', ['sass']);
	grunt.resgisterTasks('default', ['browserSync', 'watch'])
	grunt.resgisterTasks('img:compress', ['imagemin'])
};